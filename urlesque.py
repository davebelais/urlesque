"""
@author: David Belais
A fast, object-oriented alternative to urllib.parse.urlparse().
"""
import os
from collections import OrderedDict, Iterable
import re
from copy import copy, deepcopy
from numbers import Number
from typing import Optional, Dict, Union
from urllib.parse import urlencode, parse_qsl


class Undefined:

    __hash__ = 0


UNDEFINED = Undefined()

URL_REGEX = re.compile(
    (
        r'^\s*' +
        r'(?:(\w+)://)?' +  # protocol
        r'(?:/*(\w):[\\/])?' +  # drive
        r'(?:(.+?)?(?::(.+))?@)?' +  # user,password
        r'([/\\]*[^:/\\\.\?][^:/\\\?]*)?' +  # host
        r'(?::([\d]+))?' +  # port
        r'([^\?]*[^\?\\/])?[\\/]*' +  # path
        r'(?:\?([^#]+))?' +  # query string
        r'(?:#(.+))?' +  # query string
        r'\s*'
    ),
    flags=re.IGNORECASE
)


class Query(OrderedDict):

    def __init__(self, q=None):
        # type: (Optional[Union[str, Iterable[str, str], Dict]]) -> None
        self._string = None
        if isinstance(q, str):
            for k, v in parse_qsl(q):
                self[k] = v
            self._string = q
        elif q is not None:
            super().__init__(q)

    def __copy__(self):
        c = self.__class__([
            (k, v) for k, v in self.items()
        ])
        c._string = self._string
        return c

    def __setitem__(self, key, value):
        # type: (str, str) -> None
        self._string = None
        super().__setitem__(key, value)

    def __delitem__(self, key):
        # type: (str) -> None
        self._string = None
        super().__delitem__(key)

    def __str__(self):
        if self._string is None:
            return urlencode(
                [
                    (k, v)
                    for k, v in self.items()
                ],
                doseq=True
            )
        else:
            return self._string

    def __add__(self, b):
        # type: (Union[Query, str, Sequence[Tuple], Dict]) -> Query
        if not isinstance(b, self.__class__):
            b = self.__class__(b)
        a = copy(self)
        for k, bv in b.items():
            if k in a:
                av = a[k]
                if bv != av:
                    if (not isinstance(bv, Iterable)) or isinstance(bv, (str, bytes)):
                        bv = [bv]
                    else:
                        bv = list(bv)
                    if (not isinstance(av, Iterable)) or isinstance(av, (str, bytes)):
                        av = [av]
                    else:
                        av = list(av)
                    a[k] = tuple(av + bv)
            else:
                a[k] = bv
        return a



class URL:
    r"""
    A class for parsing, editing, and/or creating URLs.

    >>> print(URL('https://sub.domain.com', protocol=''))
    //sub.domain.com

    >>> print(URL('https://user:password@sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor'))
    https://user:password@sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor

    >>> print(URL('user:password@sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor'))
    //user:password@sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor

    >>> print(URL('sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor'))
    //sub.domain.com:999/directory/sub-directory?a=1&b=2&c=3#anchor

    >>> print(URL('sub.domain.com/directory/sub-directory?a=1&b=2&c=3#anchor'))
    //sub.domain.com/directory/sub-directory?a=1&b=2&c=3#anchor

    Convert a file path or non-standard URL to a properly formatted URL.

        >>> print(str(URL(r'C:\dir\sub')))
        C:/dir/sub

        >>> print(str(URL(r'\\network\dir\sub')))
        //network/dir/sub

        >>> print(str(URL(r'//network/dir/sub')))
        //network/dir/sub

        >>> print(str(URL(r'./dir/sub')))
        ./dir/sub

        >>> print(str(URL(r'../dir/sub')))
        ../dir/sub

    Optional arguments, such as `protocol`,  can be passed to alter componenets
    of the URL:

        >>> print(str(URL(r'C:\dir\sub', protocol='file')))
        file:///C:/dir/sub

    A dictionary can be passed to the query string:

        >>> q = OrderedDict([('a', 1),('b', 2),('c', 3)])
        >>> print(str(URL(r'sub.domain.com/bite-me', protocol='https', query=q)))
        https://sub.domain.com/bite-me?a=1&b=2&c=3

    Or the query string can be passed raw:

        >>> q='a=1&b=2&c=3'
        >>> print(str(URL(r'sub.domain.com/bite-me',protocol='https', query=q)))
        https://sub.domain.com/bite-me?a=1&b=2&c=3

    The output of this procedure is not escaped, to avoid redundant processing.
    Performing this transformation afterwards is simple, however:

        >>> import urllib.parse
        >>> print(urllib.parse.quote(str(URL('sub.domain.tld/directory with spaces'))))
        //sub.domain.tld/directory%20with%20spaces
    """

    def __init__(
        self,
        url: Optional[Union[str, 'URL']]=UNDEFINED,
        protocol: Optional[str]=UNDEFINED,
        drive: Optional[str]=UNDEFINED,
        user: Optional[str]=UNDEFINED,
        password: Optional[str]=UNDEFINED,
        host: Optional[str]=UNDEFINED,
        port: Optional[Union[str, int]]=UNDEFINED,
        path: Optional[str]=UNDEFINED,
        query: Optional[Union[Dict[str, Union[str, Number]], str]]=UNDEFINED,
        anchor: Optional[str]=UNDEFINED
    ):
        self._query = None
        self._path = None
        self._port = None
        self.index = -1
        self.protocol = None
        self.drive = None
        self.user = None
        self.password = None
        self.host = None
        self.port = None
        self.path = None
        self.anchor = None
        self.relative = None
        if url is not UNDEFINED:
            self.url = url
        if protocol is not UNDEFINED:
            self.protocol = protocol
        if drive is not UNDEFINED:
            self.drive = drive
        if user is not UNDEFINED:
            self.user = user
        if password is not UNDEFINED:
            self.password = password
        if host is not UNDEFINED:
            self.host = host
        if port is not UNDEFINED:
            self.port = port
        if path is not UNDEFINED:
            self.path = path
        if query is not UNDEFINED:
            self.query = query
        if anchor is not UNDEFINED:
            self.anchor = anchor

    @property
    def url(self):
        parts = []
        if self.protocol:
            parts.append(self.protocol + ':')
        if self.drive:
            if self.protocol:
                parts.append('///')
            parts.append(self.drive+':/')
        elif not self.relative:
            parts.append('//')
        if self.user:
            parts.append(self.user)
            if self.password:
                parts.append(':'+self.password)
            parts.append('@')
        if self.host:
            parts.append(self.host)
        if self.port is not None:
            if not self.host:
                parts.append('localhost')
            parts.append(':'+str(self.port))
        if self.path:
            if self.host:
                parts.append('/')
            parts.append(self.path)
        if self.query:
            parts.append(
                '?' + str(self.query)
            )
        if self.anchor:
            parts.append('#'+self.anchor)
        return ''.join(parts)

    @url.setter
    def url(self, url):
        # type: (Union[str, URL]) -> None
        if isinstance(url, URL):
            self._query = copy(url._query)
            self._port = url._port
            self._path = url._path
            self.index = url.index
            self.protocol = url.protocol
            self.drive = url.drive
            self.user = url.user
            self.password = url.password
            self.host = url.host
            self.anchor = url.anchor
            self.relative = url.relative
        else:
            m = URL_REGEX.match(url)
            if m is not None:
                (
                    self.protocol,
                    self.drive,
                    self.user,
                    self.password,
                    self.host,
                    self.port,
                    self.path,
                    self.query,
                    self.anchor
                ) = (
                    (
                        g.replace('\\', '/')
                        if isinstance(g, str)
                        else g
                    )
                    for g in m.groups()
                )
                # For file URLs and relative paths, we don't infer a host...
                if (
                    (
                        self.drive is not None
                    ) or (
                        (self.protocol is not None) and
                        self.protocol.lower() == 'file'
                    ) or (
                        not self.host
                    ) or (
                        self.host and (
                            '.' not in self.host
                        )
                    )
                ):
                    if self.host and (
                        self.host[:2] == '//' or
                        (self.host[0] == '/' and os.name == 'posix')
                    ):
                        self.relative = False
                    elif (
                        (not self.protocol) and
                        (not self.drive)
                    ):
                        self.relative = True
                    h = (
                        self.host.lstrip('/ ')
                        if self.host
                        else None
                    )
                    self.host = None
                    self.path = (
                        h+'/' + self.path
                        if (h and self.path)
                        else (h or self.path)
                    )
                elif self.host:
                    self.host = self.host.lstrip('/ ')

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self,p):
        if p is None:
            self._port = None
        else:
            self._port = int(p)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, p):
        if p:
            self._path = p.strip('\\/ ').replace('\\', '/')
        else:
            self._path = None

    @property
    def query(self):
        return self._query

    @query.setter
    def query(
        self,
        q: Union[Undefined, str, Dict[str, Union[Number, str]]]=UNDEFINED
    ):
        # TODO: Create `class Query(OrderedDict)` in order to preserve exact string when parsing
        if q is None:
            self._query = None
        else:
            self._query = Query(q)

    def __iter__(self):
        for part in (
            self.protocol,
            self.drive,
            self.user,
            self.password,
            self.host,
            self.port,
            self.path,
            self.query,
            self.anchor
        ):
            yield part

    def __str__(self):
        return self.url

    def __eq__(self,other):
        return (
            isinstance(other,self.__class__) and
            (str(self) == str(other))
        )

    def __ne__(self,other):
        return not self == other

    def __lt__(self,other):
        if not isinstance(other,(self.__class__,str)):
            raise ValueError(
                'Cannot compare an object of type `%s` with `%s`.' % (
                    self.__class__.__name__,
                    type(other).__name__
                )
            )
        return str(self) < str(other)

    def __gt__(self,other):
        if not isinstance(other,(self.__class__,str)):
            raise ValueError(
                'Cannot compare an object of type `%s` with `%s`.' % (
                    self.__class__.__name__,
                    type(other).__name__
                )
            )
        return str(self) > str(other)

    def __le__(self,other):
        if not isinstance(other,(self.__class__,str)):
            raise ValueError(
                'Cannot compare an object of type `%s` with `%s`.' % (
                    self.__class__.__name__,
                    type(other).__name__
                )
            )
        return str(self) <= str(other)

    def __ge__(self,other):
        if not isinstance(other, (self.__class__, str)):
            raise ValueError(
                'Cannot compare an object of type `%s` with `%s`.' % (
                    self.__class__.__name__,
                    type(other).__name__
                )
            )
        return str(self) >= str(other)

    def __copy__(self):
        u = URL()
        u._query = copy(self._query)
        u._port = self._port
        u._path = self._path
        u.index = self.index
        u.protocol = self.protocol
        u.drive = self.drive
        u.user = self.user
        u.password = self.password
        u.host = self.host
        u.anchor = self.anchor
        u.relative = self.relative
        return u

    def __add__(self, other: Union[str, 'URL']):
        r"""
        >>> print(URL(r'\\root\subdirectory') + r'\123\456.png')
        //root/subdirectory/123/456.png

        >>> print(URL(r'file://') + r'Z:\ABC\Directory\SubDirectory\SD\123-456-789_ABC XYZ\File.ext')
        file:///Z:/ABC/Directory/SubDirectory/SD/123-456-789_ABC XYZ/File.ext

        >>> print(URL('http://sub-domain.domain.top-level-domain/') + 'directory' + 'sub-directory/file.html')
        http://sub-domain.domain.top-level-domain/directory/sub-directory/file.html

        >>> print(URL(r'Z:\ABC\Directory\SubDirectory\SD\123-456-789_ABC XYZ\File.ext'))
        Z:/ABC/Directory/SubDirectory/SD/123-456-789_ABC XYZ/File.ext

        >>> print(URL('//sub.domain.com/directory/sub-directory/') + 'a' + 'b' + 'c')
        //sub.domain.com/directory/sub-directory/a/b/c

        >>> print(URL('//a/b/c?z=1&y=2&x=3') + 'd?z=0&v=-2&u=-3')
        //a/b/c/d?z=1&z=0&y=2&x=3&v=-2&u=-3

        >>> import urllib.parse
        >>> print(urllib.parse.quote(str(URL('sub.domain.tld') + 'dir/with spaces')))
        //sub.domain.tld/dir/with%20spaces

        >>> print(URL('subdirectory') + 'directory')
        subdirectory/directory

        >>> print(str(URL('//sub.domain.com/directory/sub-directory/') + 'a' + 'b' + 'c'))
        //sub.domain.com/directory/sub-directory/a/b/c
        """
        a = copy(self)
        b = URL(other)
        if b.host or b.path or b.drive or b.query:
            if b.protocol:
                raise ValueError(
                    'Only the base path can specify a protocol.'
                )
            if b.drive:
                if a.drive or a.port or a.host or a.path:
                    raise ValueError(
                        'Only the base path can contain a drive letter.'
                    )
                else:
                    a.drive = b.drive
            if b.query is not None:
                if a.query is None:
                    a.query = b.query
                else:
                    a.query += b.query
            other = ''
            if b.host:
                if not (a.host or a.path):
                    a.host = b.host
                else:
                    other = b.host
            if b.path:
                if other:
                    other += '/' + b.path
                else:
                    other = b.path
            if other:
                if a.path:
                    a.path += '/' + other
                else:
                    a.path = other
        return a

if __name__=='__main__':
    import doctest
    doctest.testmod()
    # print(Query('z=1&y=2&x=3') + 'z=0&v=-2&u=-3')
